// ----- TASK 1 ----- //

// Дві компанії вирішили об'єднатись, і для цього їм потрібно об'єднати базу даних своїх 
// клієнтів. Є 2 масиви рядків, у кожному з них – прізвища клієнтів. Створіть на їх основі 
// один масив, який буде об'єднання двох масивів без повторюваних прізвищ клієнтів.
const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];
// створюємо новий сет, щоб залишити лише унікальні значення масиву, що створиться після 
// об'єднання масиву clients1 з масивом clients2 за допомогою деструктуризації
const clientsMerge = new Set([...clients1, ...clients2]);
console.log(clientsMerge);

// ----- TASK 2 ----- //

const characters = [
    {
      name: "Елена",
      lastName: "Гилберт",
      age: 17, 
      gender: "woman",
      status: "human"
    },
    {
      name: "Кэролайн",
      lastName: "Форбс",
      age: 17,
      gender: "woman",
      status: "human"
    },
    {
      name: "Аларик",
      lastName: "Зальцман",
      age: 31,
      gender: "man",
      status: "human"
    },
    {
      name: "Дэймон",
      lastName: "Сальваторе",
      age: 156,
      gender: "man",
      status: "vampire"
    },
    {
      name: "Ребекка",
      lastName: "Майклсон",
      age: 1089,
      gender: "woman",
      status: "vampire"
    },
    {
      name: "Клаус",
      lastName: "Майклсон",
      age: 1093,
      gender: "man",
      status: "vampire"
    }
  ];

// Створіть на його основі масив charactersShortInfo, що складається з об'єктів, 
// у яких є тільки 3 поля - ім'я, прізвище та вік
// Використаємо метод map для проходження циклом по значенням масиву characters та
// в параметр колбек функції передамо деструктуровані змінні name, lastName, age кожного об'єкту
const charactersShortInfo = characters.map( ({name, lastName, age}) => ({name, lastName, age}) );
console.log(charactersShortInfo);

// ----- TASK 3 ----- //

const user1 = {
    name: "John",
    years: 30,
};
// Напишіть деструктуруюче присвоєння, яке: властивість name присвоїть в змінну ім'я
// властивість years присвоїть в змінну вік властивість isAdmin присвоює в змінну isAdmin
// false, якщо такої властивості немає в об'єкті

let {name: firstName, years: age, isAdmin = false} = user1;

let userContainer = document.createElement('div');
let answerOnQuestion = document.getElementById('answer');
answerOnQuestion.after(userContainer);
let task3Text = document.createElement('h3');
task3Text.textContent = 'Task 3: Виведіть змінні на екран:';
let firstNameSpan = document.createElement('span');
let ageSpan = document.createElement('span');
let isAdminSpan = document.createElement('span');

firstNameSpan.textContent = `Name: ${firstName}; `;
ageSpan.textContent = `Age: ${age}; `;
isAdminSpan.textContent = `Admin: ${isAdmin}`;

userContainer.append(task3Text);
userContainer.append(firstNameSpan);
userContainer.append(ageSpan);
userContainer.append(isAdminSpan);

// ----- TASK 4 ----- //

const satoshi2020 = {
    name: 'Nick',
    surname: 'Sabo',
    age: 51,
    country: 'Japan',
    birth: '1979-08-21',
    location: {
      lat: 38.869422, 
      lng: 139.876632
    }
  }
  
  const satoshi2019 = {
    name: 'Dorian',
    surname: 'Nakamoto',
    age: 44,
    hidden: true,
    country: 'USA',
    wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
    browser: 'Chrome'
  }
  
  const satoshi2018 = {
    name: 'Satoshi',
    surname: 'Nakamoto', 
    technology: 'Bitcoin',
    country: 'Japan',
    browser: 'Tor',
    birth: '1975-04-05'
  }

let fullProfile = {...satoshi2018, ...satoshi2019, ...satoshi2020};
console.log(fullProfile);
console.log(Object.keys(fullProfile).length);

// ----- TASK 5 ----- //

// потрібно додати до масиву ще одну книгу, не змінюючи існуючий масив 
// (в результаті операції має бути створено новий масив).
const books = [{
    name: 'Harry Potter',
    author: 'J.K. Rowling'
  }, {
    name: 'Lord of the rings',
    author: 'J.R.R. Tolkien'
  }, {
    name: 'The witcher',
    author: 'Andrzej Sapkowski'
  }];
  
const bookToAdd = {
    name: 'Game of thrones',
    author: 'George R. R. Martin'
}

let booksWithNewBook = [...books, bookToAdd];
console.log(booksWithNewBook);

// ----- TASK 6 ----- //

// Даний об'єкт employee. Додайте до нього властивості age і salary, 
// не змінюючи початковий об'єкт (має бути створено новий об'єкт,
// який включатиме всі необхідні властивості). Виведіть новий об'єкт у консоль.
const employee = {
  name: 'Vitalii',
  surname: 'Klichko'
}

let additionalProperties = {
    age: 51,
    salary: 60000
}

const updatedEmployee = {...employee, ...additionalProperties};
console.log(updatedEmployee);

// ----- TASK 7 ----- //

const array = ['value', () => 'showValue'];

let [value, showValue] = array;

alert(value); // має бути виведено 'value'
alert(showValue());  // має бути виведено 'showValue'


